FROM openjdk
WORKDIR /app
COPY target/FileManager-1.0.0.jar FileManager.jar

CMD ["java","-jar","FileManager.jar"]