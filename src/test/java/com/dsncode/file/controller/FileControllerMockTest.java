package com.dsncode.file.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.dsncode.file.handler.RestExceptionControllerAdvice;
import com.dsncode.file.service.FileService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author daniel silva
 */
@RunWith(MockitoJUnitRunner.class)
public class FileControllerMockTest {

	@Spy
	ObjectMapper om = new ObjectMapper();
	
	private MockMvc mockMvc;
	private String URL;
	
	@Mock
	FileService fileService;
	
	@InjectMocks
	FileController fileController = new FileController();
	
	@InjectMocks
	RestExceptionControllerAdvice advice= new RestExceptionControllerAdvice();
	
	String existentFileId;
	String unExistentFileId;
	
	@Before
	  public void setUp() throws NoSuchFieldException, IllegalAccessException, FileNotFoundException {
		existentFileId = "123";
		unExistentFileId = "000";
	    mockMvc = MockMvcBuilders.standaloneSetup(fileController).setControllerAdvice(advice).build();
	    JsonNode ans = om.createObjectNode().put("id",existentFileId).put("action", "deleted");
	    
	    Mockito.when(fileService.delete(existentFileId))
	    .thenReturn(ans);
	    
	    Mockito.when(fileService.delete(unExistentFileId))
	    .thenThrow(new FileNotFoundException("file "+unExistentFileId+" does not exist"));
	    
	    
	    URL = "/v1/file";
	  }
	
	
	@Test
	public void controller_must_be_online() throws Exception
	{
		mockMvc.perform(MockMvcRequestBuilders.get(URL+"/health"))
		.andExpect(status().isOk())
		.andReturn();
		
	}
	
	@Test
	public void controller_should_delete_an_existent_image() throws Exception
	{
		mockMvc.perform(MockMvcRequestBuilders.delete(URL+"/"+existentFileId))
		.andDo(print())
		.andExpect(status().isOk())
		.andReturn();
	}
	
	@Test
	public void controller_should_404_when_trying_to_delete_an_unexistent_image() throws Exception
	{
		mockMvc.perform(MockMvcRequestBuilders.delete(URL+"/"+unExistentFileId))
		.andDo(print())
		.andExpect(status().isNotFound())
		.andReturn();
	}
	
	
}
