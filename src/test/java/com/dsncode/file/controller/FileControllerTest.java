package com.dsncode.file.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dsncode.Application;
import com.dsncode.file.controller.FileController;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootApplication
@SpringBootTest(classes=Application.class,webEnvironment =WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class FileControllerTest {
	
	ObjectMapper om = new ObjectMapper();
	
	@Autowired
	private WebApplicationContext wac;
	
	private MockMvc mockMvc;
	private String URL;
    
		
	 @Before
	  public void setUp() throws NoSuchFieldException, IllegalAccessException {
	    mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	    URL = "/v1/file";
	  }
	
	
	@Test
    public void file_controller_should_be_online() throws Exception {
		
		mockMvc.perform(MockMvcRequestBuilders.get(URL+"/health"))
		.andDo(print())
		.andExpect(status().isOk())
		.andReturn();
		
//		String URL ="http://localhost:"+port+"/rest";
//		JsonNode response = this.restTemplate.getForObject(URL+"v1/file/health", JsonNode.class);
//        assert(response.path("status").asText().equals("ok"));
    }
	
	@Test
	public void test_should_be_able_to_upload_file_check_it_then_delete_it() throws Exception {
		
		final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("doge.jpeg");
	    final MockMultipartFile doge_pic = new MockMultipartFile("file", "doge.jpeg", "image/jpeg", inputStream);

	    //upload image
	    final MvcResult result = mockMvc.perform(fileUpload(URL).file(doge_pic))
	    		.andDo(print())
	            .andExpect(status().isOk())
	            .andReturn();
	    
	    JsonNode response = om.readTree(result.getResponse().getContentAsString());
	    String id = response.path("id").asText();
	    
	    //retrieve image
	    mockMvc.perform(MockMvcRequestBuilders.get(URL+"/"+id))
	    .andDo(print())
	    .andExpect(content().contentType(MediaType.IMAGE_JPEG))
	    .andReturn();
	    
	    //delete the image
	    mockMvc.perform(MockMvcRequestBuilders.delete(URL+"/"+id))
	    .andDo(print())
	    .andExpect(status().isOk())
	    .andReturn();
	    
	}
	
	
	
}
