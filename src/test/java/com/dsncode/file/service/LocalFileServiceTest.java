package com.dsncode.file.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.dsncode.Application;
import com.dsncode.file.service.FileBean;
import com.dsncode.file.service.FileService;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author daniel
 *
 */
@RunWith(SpringRunner.class)
@SpringBootApplication
@SpringBootTest(classes=Application.class)
@ActiveProfiles("test")
public class LocalFileServiceTest {

	
	
	@Autowired
	FileService fileService;
	
	static String fileId;
	
	@Test
	public void saveTest() throws IOException
	{
		File file = new File("sample/doge.jpg");
		InputStream inputStream = new FileInputStream(file);
		JsonNode response = fileService.save("doge.jpg",MediaType.IMAGE_PNG_VALUE, inputStream);
		fileId =response.path("id").asText();
		System.out.println(fileId);
		
	}
	
	@Test
	public void getFileTest() throws IOException
	{
		FileBean fileBean = fileService.get(fileId);
		assert(fileBean.getStream().available()>0);
		
	}
	
	@Test
	public void deleteTest() throws FileNotFoundException
	{
		fileService.delete(fileId);
	}
	
	
	
	
	
}
