package com.dsncode.file.controller;


import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.dsncode.file.service.FileBean;
import com.dsncode.file.service.FileService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author daniel
 *
 */
@RestController
@RequestMapping("/v1/file")
public class FileController {

	@Autowired
	ObjectMapper om;
	
	@Autowired
	FileService fileService;
	
	@RequestMapping(method=RequestMethod.POST)
    public JsonNode saveFile(@RequestParam("file") MultipartFile file,
            RedirectAttributes att) throws IOException {
		try
		{
			
			return fileService.save(file.getOriginalFilename(),file.getContentType(), file.getInputStream());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return om.createObjectNode().put("msg", e.getMessage());
		}
	
	}
	
	@RequestMapping(value="/health",method=RequestMethod.GET)
	public JsonNode health() {
		
		return om.createObjectNode().put("status","ok");
	}
	
	@RequestMapping(value="/{fileUUID:.+}",method=RequestMethod.GET)
	public ResponseEntity<byte[]> getFile(@PathVariable("fileUUID") String fileUUID) throws IOException
	{
		FileBean fileBean= fileService.get(fileUUID);
		final HttpHeaders headers = new HttpHeaders();
	    headers.set("Content-Type",fileBean.getContentType());

	    return new ResponseEntity<byte[]>(IOUtils.toByteArray(fileBean.getStream()), headers, HttpStatus.OK);
	}	
	
	@RequestMapping(value="/{fileUUID:.+}",method=RequestMethod.DELETE)
	public JsonNode deleteFile(@PathVariable("fileUUID") String fileUUID) throws FileNotFoundException
	{
		return fileService.delete(fileUUID);
	}
	
	
	@RequestMapping(value="/meta/{fileUUID:.+}",method=RequestMethod.GET)
	public JsonNode getMeta(@PathVariable("fileUUID") String fileUUID) throws FileNotFoundException
	{
		return fileService.getMeta(fileUUID);
	}
	
	
}
