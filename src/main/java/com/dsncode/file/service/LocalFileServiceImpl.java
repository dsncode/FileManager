package com.dsncode.file.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.io.Files;

@Service("localFileService")
public class LocalFileServiceImpl implements FileService{

	@Value("${file.db}")
	String fileFolder;
	
	@Autowired
	ObjectMapper om;
	
	private JsonNode buildMeta(String name,String contentType)
	{
		ObjectNode meta = om.createObjectNode();
		String id = java.util.UUID.randomUUID().toString();
		String extension = Files.getFileExtension(name);
		return meta.put("id", id).put("extension", extension).put("localName", id+"."+extension).put("contentType", contentType);
	}
	
	
	@Override
	public JsonNode save(String name, String contentType, InputStream fileStream) throws IOException {
		
		JsonNode meta = buildMeta(name,contentType);
		File targetFile = new File(fileFolder+"/"+meta.get("localName").asText());
	    FileUtils.copyInputStreamToFile(fileStream, targetFile);
	    //save meta
	    om.writeValue(new File(fileFolder+"/"+meta.get("id").asText()+".json"), meta);
		return meta;
	}

	@Override
	public JsonNode delete(String uuid) throws FileNotFoundException {
		
		JsonNode meta = getMeta(uuid);
		
		File file = new File(fileFolder+"/"+meta.path("localName").asText());
		File metaFile = new File(fileFolder+"/"+uuid+".json");
		if(!file.exists())
			throw new FileNotFoundException("file does not exist");
		
		file.delete();
		metaFile.delete();
		return om.createObjectNode().put("id", uuid).put("action", "delete").put("result", "ok");
	}

	@Override
	public FileBean get(String uuid) throws FileNotFoundException {
		JsonNode meta = getMeta(uuid);
		InputStream inputStream = new FileInputStream(new File(fileFolder+"/"+meta.path("localName").asText()));
		return new FileBean(inputStream,meta);
	}


	@Override
	public JsonNode getMeta(String uuid) throws FileNotFoundException {
		File meta = new File(fileFolder+"/"+uuid+".json");
		
		if(!meta.exists())
			throw new FileNotFoundException("file "+uuid+" does not exist");
		
		JsonNode json_meta;
		try {
			json_meta = om.readTree(meta);
		} catch (JsonProcessingException e) {
			json_meta = om.createObjectNode().put("msg", e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			json_meta = om.createObjectNode().put("msg", e.getMessage());
			e.printStackTrace();
		}
		return json_meta;
	}
	
	

}
