package com.dsncode.file.service;

import java.io.InputStream;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author daniel
 *
 */
public class FileBean {

	private InputStream fileStream;
	private JsonNode meta;
	
	public FileBean(InputStream fileStream, JsonNode meta)
	{
		this.fileStream=fileStream;
		this.meta=meta;
	}

	public InputStream getStream()
	{
		return fileStream;
	}
	
	
	public String getId()
	{
		return meta.path("id").asText();
	}
	
	public String getContentType()
	{
		return meta.path("contentType").asText();
	}
	
	public String getLocalName()
	{
		return meta.path("localPath").asText();
	}
	
	public String getExtension()
	{
		return meta.path("extension").asText();
	}
	
}
