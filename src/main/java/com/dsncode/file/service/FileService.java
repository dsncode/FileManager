
package com.dsncode.file.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * @author daniel
 *
 */
public interface FileService {
	
	public JsonNode save(String name, String contentType, InputStream fileStream) throws IOException;
	public JsonNode delete(String uuid) throws FileNotFoundException;
	public FileBean get(String uuid) throws FileNotFoundException;
	public JsonNode getMeta(String uuid) throws FileNotFoundException;

}
