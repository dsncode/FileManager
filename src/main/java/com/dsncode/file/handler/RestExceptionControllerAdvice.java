package com.dsncode.file.handler;

import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author daniel
 *
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionControllerAdvice {

	@Autowired
	ObjectMapper om;
	
	@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="file not found on the server.")
	@ExceptionHandler(FileNotFoundException.class)
	public @ResponseBody JsonNode handleFileNotFoundException(HttpServletRequest request, Exception ex) {
		
		
		return om.createObjectNode().put("msg", ex.getMessage());
		
	}
	
	@ResponseStatus(value=HttpStatus.INTERNAL_SERVER_ERROR, reason="IOException occured")
	@ExceptionHandler(IOException.class)
	public @ResponseBody JsonNode handleIOException(HttpServletRequest request, Exception ex){
		return om.createObjectNode().put("msg", ex.getMessage());
	}
	
	
	
	
	
}
